const gulp = require("gulp"),
  concat = require("gulp-concat"),
  cleanCSS = require("gulp-clean-css"),
  uglify = require("gulp-uglify"),
  imagemin = require("gulp-imagemin"),
  htmlmin = require("gulp-htmlmin"),
  nunjucksRender = require("gulp-nunjucks-render"),
  glob = require("glob");

// gulp.task("html", () => {
//   return gulp
//     .src("./Gravity/**/*html")
//     .pipe(htmlmin())
//     .pipe(gulp.dest("./dist/"));
// });

gulp.task("css", () => {
  return gulp
    .src(["./Gravity/framework/**/*css", "./Gravity/framework/css/**/*map"])
    .pipe(concat("main.css"))
    .pipe(cleanCSS())
    .pipe(gulp.dest("./dist/css"));
});

gulp.task("page-css", () => {
  return gulp
    .src("./page/**/*.css")
    .pipe(cleanCSS())
    .pipe(gulp.dest("./dist/page/"));
});

gulp.task("js-jquery", () => {
  return gulp
    .src([
      "./Gravity/framework/js/jquery.js",
      "./Gravity/framework/js/jquery-migrate.min.js",
      "./Gravity/framework/js/jquery.pagepiling.min.js",
    ])
    .pipe(concat("allJquery.js"))
    .pipe(uglify())
    .pipe(gulp.dest("./dist/js/"));
});

gulp.task("js", () => {
  return gulp
    .src([
      "./Gravity/framework/js/popper.min.js",
      "./Gravity/framework/js/bootstrap.min.js",
      "./Gravity/framework/js/contact-form.js",
      "./Gravity/framework/js/gmap3.min.js",
      "./Gravity/framework/js/remodal.min.js",
      "./Gravity/framework/lightcase/*.js",
      "./Gravity/framework/js/functions.js",
      "./Gravity/framework/js/core.min.js",
      "./Gravity/framework/js/widget.min.js",
      "./Gravity/framework/js/mouse.min.js",
      "./Gravity/framework/js/slider.min.js",
      "./Gravity/framework/js/jquery-ui-touch-punch.min.js",
      "./Gravity/framework/js/accounting.min.js",
      "./Gravity/framework/js/price-slider.min.js",
      "./Gravity/framework/js/ion.rangeSlider.min.js",
      "./Gravity/framework/js/jquery.waitforimages.js",
      "./Gravity/framework/js/isotope.pkgd.min.js",
      "./Gravity/framework/js/imagesloaded.js",
      "./Gravity/framework/js/tipso.min.js",
      "./Gravity/framework/js/owl.carousel.min.js",
      "./Gravity/framework/js/custom_plugin.js",
    ])
    .pipe(concat("script.js"))
    .pipe(uglify())
    .pipe(gulp.dest("./dist/js"));
});

gulp.task("image", () => {
  return gulp
    .src([
      "./Gravity/framework/**/*.jpg",
      "./Gravity/framework/**/*.jpeg",
      "./Gravity/framework/**/*.png",
      "./Gravity/framework/**/*.gif",
    ])
    .pipe(imagemin())
    .pipe(gulp.dest("./dist/images/"));
});

gulp.task("font", () => {
  return gulp
    .src([
      "./Gravity/framework/**/*.eot",
      "./Gravity/framework/**/*.svg",
      "./Gravity/framework/**/*.ttf",
      "./Gravity/framework/**/*.woff",
      "./Gravity/framework/**/*.woff2",
    ])
    .pipe(gulp.dest("./dist/fonts/"));
});

gulp.task("php", () => {
  return gulp.src("./Gravity/**/*.php").pipe(gulp.dest("./dist/php/"));
});

gulp.task("nunjucks", () => {
  var includeDirs = ["./Gravity/nunjucks/*"];
  return gulp
    .src("./Gravity/**/*.html")
    .pipe(
      nunjucksRender({
        path: includeDirs.reduce(function (dirs, g) {
          return dirs.concat(glob.sync(g));
        }, []),
      })
    )
    .pipe(gulp.dest("./dist/"));
});

gulp.task("watch", () => {
  gulp.watch("./Gravity/**/*.html", gulp.series("nunjucks"));
  gulp.watch("./Gravity/framework/**/*css", gulp.series("css"));
  gulp.watch("./page/**/*.css", gulp.series("page-css"));
  gulp.watch("./Gravity/framework/js/*js", gulp.series(["js", "js-jquery"]));
  gulp.watch(
    [
      "./Gravity/framework/**/*.jpg",
      "./Gravity/framework/**/*.jpeg",
      "./Gravity/framework/**/*.png",
      "./Gravity/framework/**/*.gif",
    ],
    gulp.series("image")
  );
  gulp.watch(
    [
      "./Gravity/framework/**/*.eot",
      "./Gravity/framework/**/*.svg",
      "./Gravity/framework/**/*.ttf",
      "./Gravity/framework/**/*.woff",
      "./Gravity/framework/**/*.woff2",
    ],
    gulp.series("font")
  );
  gulp.watch("./Gravity/**/*.php", gulp.series("php"));
});

gulp.task(
  "default",
  gulp.series([
    "nunjucks",
    "css",
    "page-css",
    "js-jquery",
    "js",
    "image",
    "font",
    "php",
    "watch",
  ])
);
